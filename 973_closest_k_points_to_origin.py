import random


def getDistance(x,y):
    return (x**2 + y**2)

def swap(_list,i,j):
    temp = _list[i]
    _list[i] = _list[j]
    _list[j] = temp
    

"""
O(n) solution that works similarly to finding a median. You choose a pivot randomly,
and split the list into distances smaller than the pivot (left set) and distances greater than the pivot (right set).

If the left set is the same size as k, then you have the k closest points to the origin. This works because
every element in the left is smaller than every element in the right set.

Elements to the left of i are in the left set, and elements to the right of j are in the right set.
"""
def myKClosest(distances, k, i,j):
    # keep the original i and j
    left = i
    right = j

    pivot_index = random.randint(i,j)

    j -= 1

    # if i<j, then we have seen every element
    while(i<=j):
        if (distances[i][0]<distances[pivot_index][0]):
            i+=1 # add distances[i] to the left set
        else:
            # need to move to right set
            swap(distances,i,j)
            j-=1 

    if( k == i+1):
        return distances[:i+1]
    if( k < i+1):
        return myKClosest(distances,k,left,i)
    else:
        return myKClosest(distances,k,i+1,right)
        
class Solution:
            
    def kClosest(self, points, K: int):
        if not points:
            return []
        if (len(points) <= K):
            return points
        distances = []
        # calculate the distances beforehand and store as a tuple of (distance,coordinate)
        for coord in points:
            distances.append((getDistance(coord[0],coord[1]),coord))
        ret = myKClosest(distances,K,0,len(distances)-1)
        return [r[1] for r in ret]

    def kClosestSlow(self,points,k):
        points = sorted(points, key=lambda x: x[0]**2 + x[1]**2)
        return points[:k]




import unittest



class Test(unittest.TestCase):
    s = None
    def setUp(self):
        global s
        s = Solution()

    def pointsEqual(self,list1,list2):
        for p in list1:
            if not (p in list2):
                return False
        return True

    def checkInput(self,list_,k):
        global s
        answer = s.kClosestSlow(list_,k)
        attempt = s.kClosest(list_,k)
        self.assertTrue(self.pointsEqual(answer,attempt))
        self.assertTrue(len(answer) == len(attempt))

    def test_case1(self):
        points = [[3,3],[5,-1],[-2,4]]
        self.checkInput(points,2)

    def test_case2(self):
        points = [(-1,-1), (1,1), (-1,6), (4,3)]
        self.checkInput(points,3)

    def test_case4(self):
        points = [(-1,1)]
        self.checkInput(points,1)

    def test_case5(self):
        points = [(0,0),(50,60),(30,30),(25,-10)]
        self.checkInput(points,3)

    def test_case6(self):
        points = [(-2,-20),(5,60),(-10,30),(25,-10)]
        self.checkInput(points,3)

    def test_case7(self):
        points = [[1,3],[-2,2]]
        self.checkInput(points,1)

unittest.main()