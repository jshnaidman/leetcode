class Solution:
    def maxSubArray(self, nums):
        return myMaxSubArray(nums,0,len(nums)-1)


"""
Returns the maximum sub array by first recursing on the left and right subarrays. 
The only third possible solution is one that passes through the middle of the list.
The answer is the maximum of those three solutions

The middle subarray is attained by taking the maximum between either the greatest sum
from the left side of the array, the right side of the array, or the right+left side of the array
"""
def myMaxSubArray(nums,i,j):
	if (i == j):
		return nums[i]
	mid_index = int((i+j)/2)

	left_sum = myMaxSubArray(nums,i,mid_index)
	right_sum = myMaxSubArray(nums,mid_index+1,j)
	
	l = mid_index
	r = mid_index+1


	# go through the left side and find the largest contiguous sum
	sum_ = 0
	max_sum_l = float("-inf")
	while(1):
		sum_ += nums[l]
		if (sum_ > max_sum_l):
			max_sum_l = sum_
		if ((l-1)<i):
			break
		l -= 1

	# go through the right side and find the largest contiguous sum
	sum_=0
	max_sum_r=float("-inf")
	while(1):
		sum_ += nums[r]
		if (sum_ > max_sum_r):
			max_sum_r = sum_
		if ((r+1)>j):
			break
		r += 1

	mid_sum = max(max_sum_r,max_sum_l, max_sum_l+max_sum_r) 

	if (left_sum >= right_sum and left_sum >= mid_sum):
		return left_sum
	if (right_sum >= left_sum and right_sum >= mid_sum):
		return right_sum
	else:
		return mid_sum




import unittest



class Test(unittest.TestCase):

    def test_case1(self):
        nums = [-1,2,3,-100,5,4,-3,2,4,-10,-20,-30] # len of 12
        answer = sum([5,4,-3,2,4])
        attempt = myMaxSubArray(nums, 0, len(nums)-1)
        self.assertEqual(answer,attempt)

    def test_case2(self):
        nums = [-2,1,-3,4,-1,2,1,-5,4]
        answer = sum([4,-1,2,1])
        attempt = myMaxSubArray(nums, 0, len(nums)-1)
        self.assertEqual(answer,attempt)

    def test_case3(self):
    	nums = [-1,-6,-9,4,-8,5,-4,2,-1,1,-8,0,1,3,1]
    	answer = 5
    	attempt = myMaxSubArray(nums, 0, len(nums)-1)
    	self.assertEqual(answer,attempt)


unittest.main()